FROM 10.252.246.60:5000/mbket-jdk8

ARG JAR_FILE=target/tanzu-poc.jar

COPY ${JAR_FILE} app.jar

CMD ["java", "-Xms256M", "-Dspring.profiles.active=${ACTIVE_PROFILE}", "-jar", "/jar/app.jar"]
