package com.poc.mbket.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.redis.RedisHealthIndicator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;

@Configuration
@EnableRedisRepositories(basePackages = "com.poc.mbket.repository.redis")
@Validated
public class RedisConfig {
    @NotEmpty
    @Value("${redis.host}")
    private String host;
    @NotEmpty
    @Value("${redis.port}")
    private Integer port;

    @Bean
    LettuceConnectionFactory lettuceConnectionFactory() {
        return new LettuceConnectionFactory(new RedisStandaloneConfiguration(host, port));
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate(LettuceConnectionFactory connectionFactory) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(connectionFactory);
        template.afterPropertiesSet();
        return template;
    }

    @Bean
    public RedisHealthIndicator redisHealthIndicator(LettuceConnectionFactory connectionFactory) {
        return new RedisHealthIndicator(connectionFactory);
    }
}
