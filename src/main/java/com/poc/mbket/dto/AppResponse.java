package com.poc.mbket.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class AppResponse {
    private Long id;
    private String respText;
    private Boolean respFlag;
}
