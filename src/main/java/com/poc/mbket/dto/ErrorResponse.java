package com.poc.mbket.dto;

import lombok.Data;

@Data
public class ErrorResponse {
    String message;
}
