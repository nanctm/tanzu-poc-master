package com.poc.mbket.feign;

import com.poc.mbket.dto.AppRequest;
import com.poc.mbket.dto.AppResponse;
import com.poc.mbket.feign.config.FeignProxyClientConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.validation.Valid;

@FeignClient(name = "OtherAppClient", configuration = FeignProxyClientConfig.class, url = "${mbket.other.app.host}")
public interface OtherAppClient {
    @PostMapping(value = "/api/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    void otherAppCreate(@RequestHeader("request_id") String requestId, @RequestBody @Valid AppRequest request);

    @GetMapping(value = "/api/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    AppResponse otherAppRetrieve(@RequestHeader("request_id") String requestId, @PathVariable("id") Long id);
}
